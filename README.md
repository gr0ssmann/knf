# knf

Files for the project "Knowledge and Freedom" by [me](https://max.pm).

The integrity of this repository can be verified using `git show --show-signature $COMMIT`. My OpenPGP public key is [0x1636BA9B](https://max.pm/gpg).

Some directories contain further READMEs.

# How to reproduce my results

Reproducing the results of this project should be simple. The directory `stats/` contains a couple of `*.R` files. These conduct the main analyses. You can use `stats/setup.R` to install suggested packages. In `stats/`:

**For experiment 1**, `experiment1.R` contains the preregistered analysis in `res1_logit_cl`.

**For experiment 2**, `experiment2.R` contains preregistered analysis in `a1`, `a2`, `a3`, `a4`. Note a correction to `a4`.

Tables are generated through `beliefs.R` and `tables2.R`. There is also a table in `experiment1.R`. Plots are generated through `plots.R`.

If you encounter any challenges whatsoever in running or understanding my code or results, please contact me immediately.

# License

The contents of this repository are licensed under CC BY 4.0. To obtain more information, [click here](https://creativecommons.org/licenses/by/4.0/).
