# knf\_fix

This directory contains a practical bugfix for subjects affected by a programming error in the original experiment. In `otree/knf_ca3/__init__.py`, payoffs were originally calculated as follows:

```python
draw = random.random() < C.P
teil_ausz = draw * C.LO + (not draw) * C.HI
```

This should have been implemented as follows:

```python
draw = random.random() < C.P / 100
teil_ausz = draw * C.LO + (not draw) * C.HI
```

Commit af451fa5, which also introduced `knf_fix`, fixed the bug in the original experiment for potential replicators. The bug was present at another location, but that location constituted "dead code" and was never run. We notified all potentially affected subjects and allowed them to check their eligibility for a re-run of Option 2 using the `knf_fix` app. In total, eleven subjects were eligible for a re-run. The use of this app was necessary for privacy considerations.

# knf\_impl1 and knf\_impl2

These apps implement the decisions of CAs.

For `knf_impl1`, the following parameters were randomly drawn in accordance with the instructions: *p* = 0.72, *k* = 10. (Since *p* ≠ 0.2, no CA decision was implemented, but it could have been.)

For `knf_impl2`, *k* = ∞. Subject #181 was randomly selected (and subject #181 is subject #213 when counting all CAs who made a decision for *k* = ∞ in all sessions and previous pilots). This subject did not intervene for this *k*.

The parameters and the subject were not drawn by me, but by another individual.
