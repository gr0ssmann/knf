from otree.api import *

import random
import time

import knf_globals
import drypage


doc = """
Knowledge and Freedom (Fix)
"""


class C(BaseConstants):
    NAME_IN_URL = "knf_fix"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1

    HI = 20
    LO = 0
    P = 20

    NOTP = 100 - P


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    with open("knf_fix_eligible") as file:
        subsession.session.vars["eligible"] = [line.strip() for line in file]

    for player in subsession.get_players():
        player.participant.vars["clerpay_amount"] = 0.00


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    is_eligible = models.BooleanField(initial=False)
    payoff_ascertained = models.BooleanField(initial=False)


class Eligibility(Page):
    @staticmethod
    def is_displayed(player):
        return not player.is_eligible

    @staticmethod
    def live_method(player, data):
        if isinstance(data, str):
            t0_len = len(player.session.vars["eligible"])

            player.session.vars["eligible"] = [
                el for el in player.session.vars["eligible"] if el not in data
            ]

            t1_len = len(player.session.vars["eligible"])

            if t1_len != t0_len:
                player.is_eligible = True

                return {0: True}

        return {0: False}


@drypage.apply(knf_globals.Default)
class Results:
    @staticmethod
    def display_hook(player):
        return player.is_eligible

    @staticmethod
    def vars_for_template(player):
        if not player.payoff_ascertained:
            draw = random.random() < C.P / 100
            teil_ausz = draw * C.LO + (not draw) * C.HI

            player.participant.vars["teil_ausz"] = teil_ausz

            player.payoff = teil_ausz
            player.participant.vars["clerpay_amount"] = float(player.payoff)

            player.payoff_ascertained = True

        return dict(
            teil_ausz=player.participant.vars["teil_ausz"],
        )


page_sequence = [Eligibility, Results]
