from otree.api import *

import random
import time

import numpy as np

import knf_globals
import drypage


doc = """
Knowledge and Freedom (Implementation: p = 0.2, k = ∞)
"""


class C(BaseConstants):
    NAME_IN_URL = "knf_impl2"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1

    HI = 20
    LO = 0
    P = 20

    NOTP = 100 - P


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    own_preference = models.IntegerField(
        label="Welche Option bevorzugen Sie?",
        choices=[[1, "<b>Option 1</b>"], [2, "<b>Option 2</b>"]],
        widget=widgets.RadioSelect,
    )


# PAGES
@drypage.apply(knf_globals.Default)
class OwnPreference:
    form_fields = ["own_preference"]
    form_model = "player"

    @staticmethod
    def before_hook(player):
        player.complete = time.time()


@drypage.apply(knf_globals.Default)
class Results:
    @staticmethod
    def vars_for_template(player):
        if player.payoff == 0:
            if player.own_preference == 1:
                teil_ausz = 15
            else:
                draw = random.random() < C.P / 100
                teil_ausz = draw * C.LO + (not draw) * C.HI

            player.participant.vars["teil_ausz"] = teil_ausz

            player.payoff = 1 + teil_ausz
            player.participant.vars["clerpay_amount"] = float(player.payoff)

        return dict(
            teil_ausz=player.participant.vars["teil_ausz"],
        )


page_sequence = [OwnPreference, Results]
