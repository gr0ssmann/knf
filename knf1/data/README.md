The file "complete.csv" contains the output of the script "redact.R". The use of such a file is necessary for the preservation of subject privacy.

The file "SHA256SUMS" contains a sha256-based hash about files, some of which may not be contained in this repository. For those files that are contained, they are merely included for ease of use; in general, the use of git's commit verification feature is recommended.
