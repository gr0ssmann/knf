The scripts in this directory sanitize and summarize free-form text fields using OpenAI's `text-davinci-003` model.

To run, you need the original data and an OpenAI API key.

Then, run "summarize.py". Then, run "sanitize.R". This creates "text.csv", a file with sanitized, summarized participant comments. The "subject" column refers to the same column in "../complete.csv".

Despite a simple fix to prevent RateLimitError, the API often errors out despite no issue with the text provided. I addressed this issue by manually re-running `summarize_text()` on failed inputs and putting the output where it belongs. Hence, the "text.csv" does not contain any errors, but it might if you re-run it for your own project.
