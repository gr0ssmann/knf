#!/usr/bin/env python3

# Note: This code was partially written by ChatGPT.
# You know, the good parts.

import glob

import openai
import pandas as pd
import time

openai.api_key = open("api_key").read().strip()


def summarize_text(text, prompt):
    time.sleep(15)  # to avoid RateLimitError

    text = text.strip().replace("\n", " ")

    if len(text) in [0, 1]:
        return ""

    try:
        model_engine = "text-davinci-003"
        prompt = f"QUESTION = {prompt}\n\nRESPONSE = {text}\n\nPrecisely summarize the RESPONSE to the QUESTION in English while removing all potentially personally identifiable information and unknown terms. Refer to the speaker as 'subject'. Summary:"

        response = openai.Completion.create(
            engine=model_engine,
            prompt=prompt,
            max_tokens=1024,
            n=20,
            stop=None,
            temperature=0.5,
        )

        summary = response.choices[0].text.strip()

        return summary
    except:
        return f"<ERROR>{text}</ERROR>"


prompt_s = "Wir würden uns dafür interessieren, warum Sie sich im heutigen Experiment so entschieden haben. Können Sie uns Ihre Motive mitteilen? Vielen Dank. Bitte gehen Sie insbesondere darauf ein, warum Sie sich wann dafür entschieden haben, (nicht) die eigene Wahl der anderen Person umzusetzen."
prompt_r = (
    "Was glauben Sie: Welche Forschungsfrage wird mit diesem Experiment adressiert?"
)


if __name__ == "__main__":
    subject = []
    statement = []
    research = []

    for fn in glob.glob("../all_apps_wide*.csv"):
        data = pd.read_csv(fn).fillna("")

        for j, row in data.iterrows():
            if row["knf_ca3.1.player.complete"] > 0:
                subject.append(row["participant.code"])
                statement.append(
                    summarize_text(row["knf_ca3.1.player.statement"], prompt_s)
                )
                research.append(
                    summarize_text(row["knf_ca3.1.player.research"], prompt_r)
                )

                print("ok", fn, j, statement[-1][0:20], research[-1][0:20])

    df = pd.DataFrame(
        {"subject": subject, "synth_statement": statement, "synth_research": research}
    )

    df.to_csv("intermediate.csv", index=False)
