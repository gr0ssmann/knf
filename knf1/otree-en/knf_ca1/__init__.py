from otree.api import *

import knf_globals
import drypage


doc = """
Knowledge and Freedom (CA), 1/3
"""


@knf_globals.amend(BaseConstants)
class C:
    NAME_IN_URL = "knf_ca1"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    p0 = models.IntegerField(min=0, max=1000)
    p10 = models.IntegerField(min=0, max=1000)
    p20 = models.IntegerField(min=0, max=1000)
    p30 = models.IntegerField(min=0, max=1000)
    p40 = models.IntegerField(min=0, max=1000)
    p50 = models.IntegerField(min=0, max=1000)
    p60 = models.IntegerField(min=0, max=1000)
    p70 = models.IntegerField(min=0, max=1000)
    p80 = models.IntegerField(min=0, max=1000)
    p90 = models.IntegerField(min=0, max=1000)
    p100 = models.IntegerField(min=0, max=1000)

    attempts = models.IntegerField(initial=0)
    watched_animation = models.IntegerField(initial=0)

    attention = models.BooleanField(
        label="",
        choices=[
            [
                True,
                "Yes, I paid full attention to the experiment. My data should be used.",
            ],
            [
                False,
                "No, I did not pay full attention to the experiment. My data should not be used.",
            ],
        ],
    )

    attention = models.BooleanField(
        label="",
        choices=[
            [
                True,
                "Yes, I paid full attention to the experiment. My data should be used.",
            ],
            [
                False,
                "No, I did not pay full attention to the experiment. My data should not be used.",
            ],
        ],
    )

    q1 = models.IntegerField(
        label="""<b>Question 1</b>: <i>p</i> is the probability for ...""",
        choices=[
            [45571, "... €" + str(C.LO) + " in <b>Option 2</b>."],
            [34258, "... €" + str(C.HI) + " in <b>Option 2</b>."],
        ],
        widget=widgets.RadioSelect,
    )
    q2 = models.IntegerField(
        label="""<b>Question 2</b>: What is the probability <i>p</i> if
                 your decisions can be implemented?""",
        choices=[
            [87113, str(int(C.P / 2)) + "%"],
            [59313, str(C.P) + "%"],
            [71718, str(int(C.P * 2)) + "%"],
            [
                17990,
                "This cannot be said yet, since <i>p</i> is determined randomly.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q3 = models.IntegerField(
        label="""<b>Question 3</b>: What does the other person know about <i>p</i>?""",
        choices=[
            [10389, "Nothing."],
            [18349, "The exact value of <i>p</i>."],
            [
                34691,
                "That <i>p</i> is determined randomly, a whole percentage between 0% and 100% is possible and every possible value of <i>p</i> is equally likely.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q4 = models.IntegerField(
        label="<b>Question 4</b>: You decide today whether for the other person either <b>Option 1</b>, <b>Option 2</b> or the other person's own choice should be implemented.",
        choices=[
            [
                78686,
                "This is not correct.",
            ],
            [
                21284,
                "This is correct.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q5 = models.IntegerField(
        label="""<b>Question 5</b>: How often is <b>Option 2</b> drawn for the other person before they can decide?""",
        choices=[
            [46377, "This is determined randomly."],
            [10941, "Not at all."],
            [67075, "As often as they want."],
        ],
        widget=widgets.RadioSelect,
    )
    q6 = models.IntegerField(
        label="""<b>Question 6</b>: If you decide that the other person's choice is overridden, would the person still make a choice between <b>Option 1</b> and <b>Option 2</b>?""",
        choices=[
            [
                72193,
                "The other person would not make any own choice anymore.",
            ],
            [
                22787,
                "The other person would still make an own choice between both options, but instead of their choice your choice today will be implemented.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q7 = models.IntegerField(
        label="""<b>Question 7</b>: How often does the other person choose?""",
        choices=[
            [
                12221,
                "Exactly once.",
            ],
            [
                58103,
                "This is determined randomly.",
            ],
        ],
        widget=widgets.RadioSelect,
    )


# PAGES
@drypage.apply(knf_globals.Default)
class Screencheck:
    timeout_seconds = 4 * 60 * 60

    def before_hook(player):
        player.participant.dropout = False


@drypage.apply(knf_globals.Default)
class Start:
    timeout_seconds = 4 * 60


@drypage.apply(knf_globals.Default)
class Elements:
    pass


@drypage.apply(knf_globals.Default)
class Beliefs:
    form_fields = [f"p{p}" for p in range(0, 110, 10)]
    form_model = "player"

    @staticmethod
    def error_message(player, values):
        last_x = 1000

        for p in range(0, 110, 10):
            if values[f"p{p}"] < 0 or values[f"p{p}"] > 1000:
                return "Invalid input."

            if values[f"p{p}"] > last_x:
                return "Your estimates cannot increase."

            last_x = values[f"p{p}"]


@drypage.apply(knf_globals.Default)
class Instructions:
    form_fields = ["q1", "q2", "q3", "q4", "q5", "q6", "q7"]
    form_model = "player"

    timeout_seconds = 12 * 60
    live_method = knf_globals.track_animation_factory()

    @staticmethod
    def error_message(player, values):
        if sum(values.values()) != 242244:
            player.attempts += 1

            return "At least one of your answers is incorrect. Please try again."

    def before_hook(player):
        player.participant.vars["quiz_immediately_correct"] = player.attempts == 0


@drypage.apply(knf_globals.Default)
class Attention:
    form_fields = ["attention"]
    form_model = "player"


page_sequence = [
    Screencheck,
    Start,
    Elements,
    Beliefs,
    Instructions,
    Attention,
]
