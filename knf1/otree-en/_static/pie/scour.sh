#!/usr/bin/env bash

find . -type f -name '*.svg' -exec bash -c "scour --no-line-breaks {} | sponge {}" \;
