from otree.api import *

import time

import knf_globals
import drypage


doc = """
Knowledge and Freedom (CA), 2/3
"""


@knf_globals.amend(BaseConstants, True)
class C:
    NAME_IN_URL = "knf_ca2"
    PLAYERS_PER_GROUP = None


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    if "k_matrix" not in subsession.session.vars:
        subsession.session.vars["k_matrix"] = knf_globals.sim_find(
            len(subsession.get_players())
        )

    for i, p in enumerate(subsession.get_players()):
        if "k_order" not in p.participant.vars:
            p.participant.vars["k_order"] = subsession.session.vars["k_matrix"][i]

        p.k = p.participant.vars["k_order"][subsession.round_number - 1]


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    treatment = models.StringField(initial="abstract")
    k = models.IntegerField()
    watched_animation = models.IntegerField(initial=0)
    time = models.FloatField()

    experience = models.LongStringField(initial="")

    n_lo = models.IntegerField(initial=0)
    n_hi = models.IntegerField(initial=0)

    belief_p = models.FloatField(
        blank=True,
        min=0,
        max=100,
        label="How do you estimate that the other person estimates <i>p</i>?",
    )
    intervene = models.IntegerField(min=0, max=2)


class Stimuli(ExtraModel):
    player = models.Link(Player)
    n = models.IntegerField()
    k = models.IntegerField()


def custom_export(players):
    yield ["subject", "n", "k"]

    for row in Stimuli.filter():
        yield [row.player.participant.code, row.n, row.k]


# PAGES
@drypage.apply(knf_globals.Default)
class Instructions:
    def display_hook(player):
        return player.round_number == 1


@drypage.apply(knf_globals.Default)
class Intervene:
    form_fields = ["belief_p", "intervene"]
    form_model = "player"

    live_method = knf_globals.track_animation_factory(Stimuli)

    @staticmethod
    def before_hook(player):
        # should always be true:
        if player.field_maybe_none("time") is not None:
            player.time = time.time() - player.time

    @staticmethod
    def vars_for_template(player):
        if player.field_maybe_none("time") is None:
            player.time = time.time()

        if player.field_maybe_none("experience") is None:
            results = []
            n_lo = 0
            n_hi = 0

            if player.k != C.INFINITY:
                for i in range(player.k):
                    if player.treatment == "abstract":
                        result = "???"
                    else:
                        if random.random() < C.P / 100:
                            result = C.LO
                            n_lo += 1
                        else:
                            result = C.HI
                            n_hi += 1

                    results.append([i + 1, result])

            if player.treatment != "abstract":
                player.experience = ",".join(str(r[1]) for r in results)
                player.n_lo = n_lo
                player.n_hi = n_hi

        return dict(
            results=enumerate(player.experience.split(","), 1),
            n_lo=player.n_lo,
            n_hi=player.n_hi,
        )


page_sequence = [
    Instructions,
    Intervene,
]
