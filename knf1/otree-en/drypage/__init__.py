import logging

from typing import Callable
from inspect import signature
from functools import partial


def extend(fun: Callable) -> Callable:
    """
    Mark function as extending one of oTree's Page methods. (The extension consists of an additional keyword argument 'page' in the function signature.)

    :param fun: The function to be marked.
    :return: the decorated function
    :rtype: Callable
    """

    fun.__extend__ = True  # Mark function for extension

    return fun


def apply(superpage: object) -> Callable:
    """
    Create decorator that applies superpage attributes to any true page and magically rewrites them to conform with the no-self format.

    :param superpage: The superpage to be applied to a true page.
    :return: a decorator that applies a particular superpage
    :rtype: Callable
    """

    if not any(b.__name__ == "Page" for b in superpage.__bases__):
        raise ValueError(
            f"Class {superpage.__name__} must subclass Page."
        )  # The superpage must subclass Page to give oTree access to certain internal attributes

    def decorator(cls: object) -> object:
        """
        This decorator monkey-patches extended functions to work with oTree's no-self format, while giving extended function the additional keyword argument 'page'.

        :param cls: The true page to be rewritten.
        :return: a page that works like a subclassed Page before the no-self format
        :rtype: Callable
        """

        if any(b.__name__ == "Page" for b in cls.__bases__):
            raise ValueError(
                f"Class {cls.__name__} must not subclass Page."
            )  # True pages must not subclass Page because the superclass already does and this would lead to issues in line 81.

        class NewPage(
            cls,
            superpage,  # Note the order of inheritance. The true page has precedence over the superpage. This enables us to warn before accidental overwrites/unexpected behavior.
        ):
            __extended__ = (
                dict()
            )  # Will contain the pre-monkey-patch versions of methods

        for name in dir(superpage):  # Go through each attribute of the superpage...
            cur_attr = getattr(superpage, name)

            if (
                hasattr(cur_attr, "__extend__")
                and getattr(cur_attr, "__extend__")
                and "page" in signature(cur_attr).parameters
            ):  # ... if this attribute extends a method in oTree...
                if hasattr(cls, name):
                    raise ValueError(
                        f"{cls.__name__}'s {name} would be overwritten because of {superpage.__name__}. Use hook method instead."
                    )  # (don't accidentally overwrite attributes in the true page with those from the superpage)

                NewPage.__extended__[
                    name
                ] = cur_attr  # ... store current method in __extended__...
                setattr(
                    NewPage, name, partial(NewPage.__extended__[name], page=cls)
                )  # ... monkey-patch extended method so that it works with oTree. Call it with the page kwarg so that method learns current page. Thanks, functools!

        NewPage.__name__ = cls.__name__  # Used for correct .html file name.
        NewPage.__module__ = cls.__module__  # Used for correct (template) directory.

        return NewPage

    return decorator
