from otree.api import Page

import copy
import random

import drypage

INFINITY = 999999  # wow...
K = [0, 1, 2, 5, 10, 25, 50, 1000, INFINITY]


class Default(Page):
    @drypage.extend
    def is_displayed(player, page):
        try:
            return page.display_hook(player)
        except AttributeError:
            return True

    @drypage.extend
    def get_timeout_seconds(player, page):
        try:
            return page.timeout_seconds
        except AttributeError:
            return 600

    @drypage.extend
    def before_next_page(player, timeout_happened, page):
        if timeout_happened:
            player.participant.dropout = True

            return

        try:
            return page.before_hook(player)
        except AttributeError:
            return

    @drypage.extend
    def app_after_this_page(player, upcoming_apps, page):
        if "dropout" in player.participant.vars and player.participant.dropout:
            player.participant.vars["clerpay_amount"] = 1

            return "clerpay_end"

        try:
            return page.app_hook(player, upcoming_apps)
        except AttributeError:
            pass


def get_proposal(n):
    return [random.sample(K, len(K)) for _ in range(n)]


def permute(matrix):
    n = len(matrix)
    g = len(K)

    n1 = random.randrange(0, n)
    g1, g2 = None, None

    while g1 == g2:
        g1, g2 = random.randrange(0, g), random.randrange(0, g)

    matrix[n1][g1], matrix[n1][g2] = matrix[n1][g2], matrix[n1][g1]

    return (n1, g1, g2)


def undo_permute(matrix, pos):
    n1, g1, g2 = pos

    matrix[n1][g1], matrix[n1][g2] = matrix[n1][g2], matrix[n1][g1]


def check_constraint(matrix):
    s = sum(K)
    g = len(K)

    for p in matrix:
        assert sum(p) == s and len(p) == g


def mean(x):
    return sum(x) / len(x)


def variance(x):
    return (mean([el ** 2 for el in x]) - mean(x) ** 2) * (len(x) / (len(x) - 1))


def balance(matrix):
    count = dict()

    # initialize count matrix
    for k in K:
        for r in range(len(K)):
            count[(k, r)] = 0

    for p in matrix:
        for r, k in enumerate(p):
            count[(k, r)] += 1

    return variance(count.values())


def sim_find(n, maxinner=500_000, maxouter=100):
    for i in range(maxouter):
        brk = False
        x_opt = get_proposal(n)
        y_opt = balance(x_opt)

        x_new = copy.deepcopy(x_opt)

        print(f"outer: {y_opt}")

        for j in range(maxinner // maxouter):
            pos = permute(x_new)
            y_new = balance(x_new)

            if y_new < y_opt:
                print(f"sim_find: improvement: {y_new:.3f}")

                x_opt = x_new
                y_opt = y_new
            else:
                undo_permute(x_new, pos)

            if y_new == 0:
                brk = True
                break

        if brk:
            break

        check_constraint(x_opt)

    return x_opt


def amend(mixin, rounds=False):
    def decorator(cls):
        class C(mixin, cls):
            HI = 20
            LO = 0
            P = 20

            INFINITY = INFINITY
            K = K

            if rounds:
                NUM_ROUNDS = len(K)

            NOTP = 100 - P

        return C

    return decorator


def track_animation_factory(stimuli=None):
    def track_animation(player, data):
        if len(data) == 3 and data[0] == "animation":
            player.watched_animation += 1

        if stimuli is not None:
            stimuli.create(player=player, n=data[1], k=data[2])

    return track_animation


if __name__ == "__init__":
    pass
