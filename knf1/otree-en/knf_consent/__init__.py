from otree.api import *


doc = """
Knowledge and Freedom (Consent)
"""


class C(BaseConstants):
    NAME_IN_URL = "knf_consent"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    consent = models.BooleanField(
        initial=False,
        label="Do you agree to the listed terms of participation?",
        choices=[
            [True, "I agree."],
            [False, "I do not agree and withdraw."],
        ],
    )


# PAGES
class Consent(Page):
    form_fields = ["consent"]
    form_model = "player"

    @staticmethod
    def app_after_this_page(player, upcoming_apps):
        if not player.consent:
            player.participant.vars["clerpay_amount"] = 1.00

            return "clerpay_end"


page_sequence = [Consent]
