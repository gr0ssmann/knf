This is the **translated English version** of the experiment. The translation was performed with OpenAI's `text-davinci-003` model, with some manual adjustments.

Remember to run `git submodule update --init --recursive` after cloning this repository.
