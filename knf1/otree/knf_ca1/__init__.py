from otree.api import *

import knf_globals
import drypage


doc = """
Knowledge and Freedom (CA), 1/3
"""


@knf_globals.amend(BaseConstants)
class C:
    NAME_IN_URL = "knf_ca1"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    p0 = models.IntegerField(min=0, max=1000)
    p10 = models.IntegerField(min=0, max=1000)
    p20 = models.IntegerField(min=0, max=1000)
    p30 = models.IntegerField(min=0, max=1000)
    p40 = models.IntegerField(min=0, max=1000)
    p50 = models.IntegerField(min=0, max=1000)
    p60 = models.IntegerField(min=0, max=1000)
    p70 = models.IntegerField(min=0, max=1000)
    p80 = models.IntegerField(min=0, max=1000)
    p90 = models.IntegerField(min=0, max=1000)
    p100 = models.IntegerField(min=0, max=1000)

    attempts = models.IntegerField(initial=0)
    watched_animation = models.IntegerField(initial=0)

    attention = models.BooleanField(
        label="",
        choices=[
            [
                True,
                "Ja, ich habe meine volle Aufmerksamkeit dem Experiment gewidmet. Meine Daten sollten verwendet werden.",
            ],
            [
                False,
                "Nein, ich habe nicht meine volle Aufmerksamkeit dem Experiment gewidmet. Meine Daten sollten nicht verwendet werden.",
            ],
        ],
    )

    attention = models.BooleanField(
        label="",
        choices=[
            [
                True,
                "Ja, ich habe meine volle Aufmerksamkeit dem Experiment gewidmet. Meine Daten sollten verwendet werden.",
            ],
            [
                False,
                "Nein, ich habe nicht meine volle Aufmerksamkeit dem Experiment gewidmet. Meine Daten sollten nicht verwendet werden.",
            ],
        ],
    )

    q1 = models.IntegerField(
        label="""<b>Frage 1</b>: <i>p</i> ist die Wahrscheinlichkeit für ...""",
        choices=[
            [45571, "... " + str(C.LO) + " € in <b>Option 2</b>."],
            [34258, "... " + str(C.HI) + " € in <b>Option 2</b>."],
        ],
        widget=widgets.RadioSelect,
    )
    q2 = models.IntegerField(
        label="""<b>Frage 2</b>: Wie hoch ist die Wahrscheinlichkeit <i>p</i>, wenn
                 Ihre Entscheidungen umgesetzt werden können?""",
        choices=[
            [87113, str(int(C.P / 2)) + "%"],
            [59313, str(C.P) + "%"],
            [71718, str(int(C.P * 2)) + "%"],
            [
                17990,
                "Das kann man noch nicht sagen, da <i>p</i> zufällig bestimmt wird.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q3 = models.IntegerField(
        label="""<b>Frage 3</b>: Was weiß die andere Person über <i>p</i>?""",
        choices=[
            [10389, "Nichts."],
            [18349, "Den genauen Wert von <i>p</i>."],
            [
                34691,
                "Dass <i>p</i> zufällig bestimmt wird, ein ganzer Prozentsatz zwischen 0% und 100% ist und jeder mögliche Wert von <i>p</i> gleich wahrscheinlich ist.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q4 = models.IntegerField(
        label="<b>Frage 4</b>: Sie entscheiden heute, ob für die andere Person entweder <b>Option 1</b>, <b>Option 2</b> oder die eigene Wahl der anderen Person umgesetzt werden soll.",
        choices=[
            [
                78686,
                "Das ist nicht richtig.",
            ],
            [
                21284,
                "Das ist richtig.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q5 = models.IntegerField(
        label="""<b>Frage 5</b>: Wie oft wird <b>Option 2</b> für die andere Person gezogen, bevor sie sich entscheiden kann?""",
        choices=[
            [46377, "Das wird zufällig bestimmt."],
            [10941, "Gar nicht."],
            [67075, "So oft, wie sie will."],
        ],
        widget=widgets.RadioSelect,
    )
    q6 = models.IntegerField(
        label="""<b>Frage 6</b>: Wenn Sie entscheiden, dass die Wahl der anderen Person überschrieben wird, träfe die Person dann überhaupt noch eine Wahl zwischen <b>Option 1</b> und <b>Option 2</b>?""",
        choices=[
            [
                72193,
                "Die andere Person träfe gar keine eigene Wahl mehr.",
            ],
            [
                22787,
                "Die andere Person träfe immer noch eine eigene Wahl zwischen beiden Optionen, aber es wird anstelle ihrer Wahl Ihre heutige Wahl umgesetzt.",
            ],
        ],
        widget=widgets.RadioSelect,
    )
    q7 = models.IntegerField(
        label="""<b>Frage 7</b>: Wie oft wählt die andere Person?""",
        choices=[
            [
                12221,
                "Genau ein mal.",
            ],
            [
                58103,
                "Das wird zufällig bestimmt.",
            ],
        ],
        widget=widgets.RadioSelect,
    )


# PAGES
@drypage.apply(knf_globals.Default)
class Screencheck:
    timeout_seconds = 4 * 60 * 60

    def before_hook(player):
        player.participant.dropout = False


@drypage.apply(knf_globals.Default)
class Start:
    timeout_seconds = 4 * 60


@drypage.apply(knf_globals.Default)
class Elements:
    pass


@drypage.apply(knf_globals.Default)
class Beliefs:
    form_fields = [f"p{p}" for p in range(0, 110, 10)]
    form_model = "player"

    @staticmethod
    def error_message(player, values):
        last_x = 1000

        for p in range(0, 110, 10):
            if values[f"p{p}"] < 0 or values[f"p{p}"] > 1000:
                return "Ungültige Eingabe."

            if values[f"p{p}"] > last_x:
                return "Ihre Schätzungen können nicht steigen."

            last_x = values[f"p{p}"]


@drypage.apply(knf_globals.Default)
class Instructions:
    form_fields = ["q1", "q2", "q3", "q4", "q5", "q6", "q7"]
    form_model = "player"

    timeout_seconds = 12 * 60
    live_method = knf_globals.track_animation_factory()

    @staticmethod
    def error_message(player, values):
        if sum(values.values()) != 242244:
            player.attempts += 1

            return "Mindestens eine Ihrer Antworten ist nicht korrekt. Bitte versuchen Sie es erneut."

    def before_hook(player):
        player.participant.vars["quiz_immediately_correct"] = player.attempts == 0


@drypage.apply(knf_globals.Default)
class Attention:
    form_fields = ["attention"]
    form_model = "player"


page_sequence = [
    Screencheck,
    Start,
    Elements,
    Beliefs,
    Instructions,
    Attention,
]
