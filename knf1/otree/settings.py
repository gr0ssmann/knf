SESSION_CONFIGS = [
    dict(
        name="KnowledgeAndFreedomCA",
        app_sequence=[
            # "clerpay_start",
            "knf_consent",
            "knf_ca1",
            "knf_ca2",
            "knf_ca3",
            # "clerpay_end",
        ],
        num_demo_participants=1,
    ),
]
