from otree.api import *

import random
import time

import knf_globals
import drypage


doc = """
Knowledge and Freedom (CA), 3/3
"""


@knf_globals.amend(BaseConstants)
class C:
    NAME_IN_URL = "knf_ca3"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    complete = models.FloatField(initial=0)

    estimate = models.IntegerField(min=0, max=100)

    own_preference = models.IntegerField(
        label="Welche Option bevorzugen Sie?",
        choices=[[1, "<b>Option 1</b>"], [2, "<b>Option 2</b>"]],
        widget=widgets.RadioSelect,
    )

    statement = models.LongStringField(blank=True, label="")
    research = models.LongStringField(blank=True, label="")

    male = models.BooleanField(
        label="Mit welchem Geschlecht identifizieren Sie sich?",
        choices=[[True, "Männlich"], [False, "Weiblich"], [False, "Divers"]],
    )
    econ = models.BooleanField(
        label="Haben Sie im Rahmen Ihres Studiums bereits eine Grundveranstaltung zur Mikroökonomik belegt?",
        choices=[[True, "Ja"], [False, "Nein"]],
    )
    abi = models.FloatField(
        label="Welche Gesamtnote haben Sie in der allgemeinen Hochschulreife („Abitur“) erlangt? Wenn Sie das nicht offenbaren möchten, oder Sie Ihre Hochschulzugangsberechtigung außerhalb des deutschen Notensystems erworben haben, geben Sie bitte 0 ein."
    )


def abi_error_message(player, value):
    if (value < 0.7 and value != 0) or value > 4:
        return "Ungültige Abiturnote."


# PAGES
@drypage.apply(knf_globals.Default)
class TheoryOfMind:
    form_fields = ["estimate"]
    form_model = "player"


@drypage.apply(knf_globals.Default)
class OwnPreference:
    form_fields = ["own_preference"]
    form_model = "player"

    @staticmethod
    def before_hook(player):
        player.complete = time.time()


@drypage.apply(knf_globals.Default)
class Demo:
    form_fields = ["male", "econ", "abi"]
    form_model = "player"


@drypage.apply(knf_globals.Default)
class Results:
    @staticmethod
    def vars_for_template(player):
        if player.payoff == 0:
            teil_relevant = 1 + (random.random() < 0.05) * 1

            if teil_relevant == 1:
                teil_ausz = 4 + 2 * player.participant.vars["quiz_immediately_correct"]
            else:
                if player.own_preference == 1:
                    teil_ausz = 15
                else:
                    draw = random.random() < C.P / 100
                    teil_ausz = draw * C.LO + (not draw) * C.HI

            player.participant.vars["teil_relevant"] = teil_relevant
            player.participant.vars["teil_ausz"] = teil_ausz

            player.payoff = 1 + teil_ausz
            player.participant.vars["clerpay_amount"] = float(player.payoff)

        return dict(
            teil_relevant=player.participant.vars["teil_relevant"],
            teil_ausz=player.participant.vars["teil_ausz"],
            quiz=player.participant.vars["quiz_immediately_correct"],
        )


@drypage.apply(knf_globals.Default)
class Statement:
    form_fields = ["statement", "research"]
    form_model = "player"

    timeout_seconds = 2 * 60 * 60


page_sequence = [TheoryOfMind, OwnPreference, Demo, Statement, Results]
