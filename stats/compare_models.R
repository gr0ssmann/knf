source("tables2.R")

source_model_here <- t2_1[[5]] # use t2_1[[5]] for "model 5, Table 4" or t2_1b[[2]] for "model 2, Table 6"
# after changing this, make sure to rerun `python model_mle.py`!

rerun_with_logit <- FALSE # set this to TRUE to get a logit model instead of LPM

# the rest runs through automatically, don't worry about it

from_source <- predict(source_model_here)
from_mle <- read.table("mle_probs.txt")$V1

if (length(from_source) != length(from_mle)) {
    stop("ERROR: You must rerun `python model_mle.py`.")
}

if (rerun_with_logit) {
    from_source <- predict(glm(source_model_here, family = binomial))
    from_source <- 1/(1 + exp(-from_source))
}

pred1 <- from_source >= 0.5
pred2 <- from_mle >= 0.5
actual <- source_model_here$model$"I(intervene" # this is just a hack to get actual outcomes

print(cor(from_source, from_mle)) # that's huge

print(xtabs(~ I(pred1 == actual) + I(pred2 == actual)))
