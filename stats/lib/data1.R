library(dplyr) # version 1.1.0
library(lmtest) # version 0.9-40
library(sandwich) # version 3.0-2

options(scipen = 1000)

source("lib/reshape_within.R")
source("lib/util.R")

reshape_knf <- function (data, col) {
    reshape_within(data,
                   "subject",
                   endsWith(names(data), "player.k"),
                   endsWith(names(data), paste0("player.", col))) %>%
    mutate(round = rep(1:9, nrow(data)),
           k = treat,
           k_factor = factor(k)) %>%
    select(-treat) %>%
    arrange(subject, k)
}

knf1 <- read.csv("../knf1/data/complete.csv")
stopifnot(all(knf1$subject == 1:nrow(knf1)))

knf1 <- mutate_at(knf1, .vars = grep("player.k$", names(knf1), value = T),
                  ~ ifelse(. == 999999, Inf, .)) %>%
        mutate(knf_ca3.1.player.own_preference = factor(knf_ca3.1.player.own_preference))

in_long <- reshape_knf(knf1, "intervene") %>% mutate(intervene = y > 0)

obtain <- function (subject_, field) knf1[subject_, field]
obtain_in <- Vectorize(function (subject_, k_, field) filter(in_long, subject == subject_, k == k_) %>% pull(field))

levels(in_long$k_factor)[9] <- "∞"

in_wide <- reshape(select(in_long, subject, k, intervene),
                   idvar = "subject",
                   timevar = "k",
                   direction = "wide")

# stim <- read.csv("../knf1/data/stimuli.csv")
