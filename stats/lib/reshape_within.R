reshape_within <- function (data, id, treatment, outcomes) {
    m <- sum(treatment)
    stopifnot(m == sum(outcomes))
    stopifnot(ncol(data[, id]) == 1)

    out <- data.frame(subject = numeric(m*nrow(data)),
                      treat = numeric(m*nrow(data)),
                      y = numeric(m*nrow(data)))
    
    for (j in 1:nrow(data)) {
        out[(j - 1) * m + 1:m, ] <- data.frame(subject = data[j, id],
                                               treat = unlist(data[j, treatment]),
                                               y = unlist(data[j, outcomes]))
    }
    
    out
}
