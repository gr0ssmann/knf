import numdifftools as nd
import numpy as np
import pandas as pd
import sys
from scipy.optimize import minimize
from scipy.stats import norm


def W(arg, ca, chooser, phi=np.nan):
    # from formal model

    if ca == 1 and chooser == 1:
        return 1 if arg == 1 else 0
    elif ca == 1 and chooser == 2:
        return phi if arg == 1 else 1 - phi
    elif ca == 2 and chooser == 1:
        return 1 - phi if arg == 1 else phi
    elif ca == 2 and chooser == 2:
        return 0 if arg == 1 else 1
    else:
        return np.nan


def degarbage(x):
    return np.where(np.isinf(x) | np.isnan(x), -1e6, x)


def negll(params, data, probs = False):
    phi, sigma = params
    plocal = np.zeros(len(data))
    i = 0

    for _, row in data.iterrows():
        belief = row["belief"]

        wx = belief * W(1, row["ca"], 1, phi) + (1 - belief) * W(1, row["ca"], 2, phi)
        wy = belief * W(2, row["ca"], 1, phi) + (1 - belief) * W(2, row["ca"], 2, phi)

        wdiff = wx - wy
        plocal[i] = norm.cdf(wdiff, scale=sigma)

        i = i + 1

    if not probs:
        lliks = (data["imposed"] == 1) * np.log(plocal) + (
            1 - (data["imposed"] == 1)
        ) * np.log(1 - plocal)

        return -degarbage(np.sum(lliks))
    else:
        return plocal


def objective(params):
    return negll(params, data)


# settings

chooser = 1

# prepare data

data_full = pd.read_csv("../knf2/data/redacted.csv")

if chooser == 1:
    col = "intervene_0"
elif chooser == 2:
    col = "intervene_inf"
elif chooser == 3:
    col = "intervene_uncertain"
elif chooser == 4:
    col = "intervene_info"
elif chooser == 5:
    col = "intervene_known"
else:
    raise ValueError

data = data_full[data_full[col] > 0][["own_preference", "belief1", col]].rename(
    columns={"own_preference": "ca", "belief1": "belief", col: "imposed"}
)

if chooser == 5:
    # chooser type is known exactly
    data["belief"] = (data_full["other"] == 1).astype(int)
else:
    data["belief"] /= 300

# estimation
# note: some warnings are completely normal; in these cases, degarbage() is triggered

initial_params = [0.5, 0.5]

bounds = [(-5, 5), (0.0001, 5)]

result = minimize(objective, initial_params, method="L-BFGS-B", bounds=bounds)

# get standard errors from Hessian

hessian_func = nd.Hessian(objective)
hessian = hessian_func(result.x)

fisher_info = np.linalg.inv(hessian)

ses = np.sqrt(np.diag(fisher_info))

# print results

print(
    f"phi\t{round(result.x[0], 3)}\tci: {round(result.x[0] - 1.96*ses[0], 3)} to {round(result.x[0] + 1.96*ses[0], 3)}"
)
print(
    f"sigma\t{round(result.x[1], 3)}\tci: {round(result.x[1] - 1.96*ses[1], 3)} to {round(result.x[1] + 1.96*ses[1], 3)}"
)

# write fitted probabilities to file

np.savetxt("mle_probs.txt", negll(result.x, data, True), delimiter=",")
