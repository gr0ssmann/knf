The directory `knf2` contains the original German-language instructions as used in the experiment.

The directory `knf2_en` contains [automatically translated](https://gitlab.com/gr0ssmann/translate) instructions in the English language. Some small changes were made manually.

*Note*: You need [uproot](https://uproot.science/) to run this experiment.
