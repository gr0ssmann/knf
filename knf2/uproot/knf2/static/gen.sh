#!/usr/bin/env bash

set -x

rm -f *.png

while read -r COLOR; do
    while read -r LETTER; do
        sed "s/800000/${COLOR}/" User_icon_2.svg |
            sed "s/Z/${LETTER}/" |
            inkscape -w 400 -h 400 /dev/stdin -o "${COLOR}+${LETTER}.png" &
    done < letters.txt
done < colors.txt

wait

while read -r COLOR; do
    while read -r LETTER; do
            optipng -quiet "${COLOR}+${LETTER}.png" &
    done < letters.txt
done < colors.txt

wait
