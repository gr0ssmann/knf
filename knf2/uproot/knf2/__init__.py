import collections
import random
import time
import wtforms

from uproot.smithereens import *
from wtforms import validators
from wtforms.fields import IntegerField, RadioField


class Constants:
    METHOD = "sepa"
    COLORS = ["0074D9", "FF4136", "B10DC9", "2ECC40", "FF851B"]
    LETTERS = list("ABCDE")
    NUMS = list("0123456789")
    USE1 = 0.01
    USE2 = 0.09
    USE3 = 0.90
    DEMO = False


def on_player(session, player):
    player.complete = 0
    player.order = random.sample(("0", "inf"), 2)
    player.suborder = random.sample((True, False), 2)
    player.suborder2 = random.sample((True, False), 2)
    player.colors = dict(zip(Constants.LETTERS, random.sample(Constants.COLORS, 5)))
    player.plus = random.choice((0, 1))
    player.other = random.choice((1, 2))
    player.payoff = 1
    player.payoffs = [0, 0, 2]
    player.payment_code = "554" + "".join(random.choices(Constants.NUMS, k=13))
    player.i = 0


class Info(Page):
    pass


class Instructions(Page):
    pass


class InterveneBasic(Page):
    def fields(page, player):
        return {
            f"intervene_{player.order[player.round-1]}": RadioField(
                "",
                choices=[0, 1, 2],
            )
        }

    def before(page, player):
        return dict(i=player.i)

    def after_always_once(page, player):
        player.i += 1


class Uncertain(Page):
    fields = {
        "intervene_uncertain": RadioField(
            "",
            choices=[0, 1, 2],
        )
    }

    def before(page, player):
        return dict(i=player.i)

    def after_always_once(page, player):
        player.i += 1


class KnownOther(Page):
    fields = {
        "intervene_known": RadioField(
            "",
            choices=[0, 1, 2],
        )
    }

    def before(page, player):
        return dict(i=player.i)

    def after_always_once(page, player):
        player.known_letter = Constants.LETTERS[player.i]
        player.i += 1


class InfoChoice(Page):
    def fields(page, player):
        if player.plus:
            extra = {
                f"intervene_info": RadioField(
                    "",
                    choices=[0, 1, 2],
                )
            }
        else:
            extra = {}

        return extra | {
            f"info_provided": RadioField(
                "",
                choices=["0", "1"],
            )
        }

    def before(page, player):
        return dict(i=player.i)

    def after_always_once(page, player):
        player.i += 1


class Results(Page):
    timeout = 1200

    def before_once(page, player):
        z = random.choices([1, 2, 3], [Constants.USE1, Constants.USE2, Constants.USE3])[
            0
        ]

        player.use_payoff1 = z == 1
        player.use_payoff2 = z == 2
        player.use_payoff3 = z == 3
        player.payoff = (
            1
            + player.use_payoff1 * player.payoffs[0]
            + player.use_payoff2 * player.payoffs[1]
            + player.use_payoff3 * player.payoffs[2]
        )

        player.complete = 1
        player.time_complete = time.time()
        player.time_spent = player.time_complete + player.time_spent

    def before(page, player):
        return dict(
            fields=[
                "intervene_0",
                "intervene_inf",
                "intervene_uncertain",
                "other",
                "intervene_known",
                "plus",
                "info_provided",
                "intervene_info",
                "own_preference",
                "belief1",
                "belief2",
            ]
        )


class OwnPreference(Page):
    fields = {
        "own_preference": RadioField(
            choices=[(1, "Option 1"), (2, "Option 2")],
        )
    }

    def after_always_once(page, player):
        player.own_preference = int(player.own_preference)

        if player.own_preference == 1:
            player.payoffs[0] = 15
        else:
            get_lo = random.random() <= 0.2
            player.payoffs[0] = get_lo * 0 + (1 - get_lo) * 20


class Belief1(Page):
    fields = {
        "belief1": IntegerField(
            validators=[validators.NumberRange(min=0, max=300)],
        )
    }

    def after_always_once(page, player):
        player.belief1 = int(player.belief1)
        true_val = 181

        pwin = max(0, min(1, 1 - 0.15 * abs(player.belief1 - true_val)))

        won = random.random() <= pwin
        player.payoffs[1] = won * 10 + (1 - won) * 0


class Belief2(Page):
    fields = {
        "belief2": IntegerField(
            validators=[validators.NumberRange(min=0, max=300)],
        )
    }

    def after_always_once(page, player):
        player.payoffs[1] += 5


class Consent(Page):
    fields = {
        "consent": RadioField(
            choices=[0, 1],
        )
    }

    def after_always_once(page, player):
        player.consent = int(player.consent)

        if player.consent == 0:
            put_on_page(player, PaymentForm)
        else:
            player.time_spent = -time.time()


class PaymentForm(Page):
    timeout = 3600


class Attention(Page):
    fields = {
        "other_memory": RadioField(
            choices=[1, 2],
        )
    }

    def after_always_once(page, player):
        player.other_memory = int(player.other_memory)

        if player.other_memory == player.other:
            player.payoffs[2] += 0.25


async def report(request, session):
    sn = session._name

    def obtain(username):
        with Log("player", sn, username) as p:
            return p.complete

    return collections.Counter(obtain(p) for p in session.members)


page_order = [
    Consent,
    Info,
    OwnPreference,  # (181/300) * 15 + (119/300) * (0.2 * 0 + 0.8 * 20)
    Belief1,
    Belief2,
    Instructions,
    Rounds(InterveneBasic, n=2),
    Random(
        Uncertain,
        InfoChoice,
        KnownOther,
    ),
    Attention,
    Results,
    PaymentForm,
]

routes = {"report": report}
