# R version: 4.2.2

# read original files

data <- NULL
session <- 1

for (file in sort(Sys.glob("knf*endstate.csv"))) {
    cat(file, fill = T)
    
    cur_data <- cbind(read.csv(file), data.frame(session = session, maastricht = (session >= 16 & session <= 17)))

    if (is.null(data)) {
        data <- cur_data
    }
    else {
        data <- rbind(data, cur_data)
    }

    session <- session + 1
}

write.csv(data, file = "complete.csv", na = "", row.names = F)

# Filter out those who are not complete (as per preregistration)
data <- data[data$complete == 1, ]

# Order by time of completion (as per preregistration)
data <- data[order(data$time_complete), ]

# Remove excess subjects (as per preregistration)
data <- head(data, n = 600)

# Assign subject ID here
data <- cbind(data, data.frame(subject = 1:nrow(data)))

# Safety check
stopifnot(all(sort(data$X_name) == unique(sort(data$X_name))))

# Remove sensitive columns
bad <- c("X_created",
         "X_name",
         "X_username",
         "X_token",
         "payment_code",
         "payoff",
         "time_complete",
         "timeouts_until",
         "use_payoff1",
         "use_payoff2",
         "use_payoff3",
         "what_ran" # not sensitive but annoying
)

redacted <- data
redacted[, bad] <- list(NULL)

write.csv(redacted, file = "redacted.csv", na = "", row.names = F)
